# An Interactive Phonemic Chart
A demo: https://enliso.herokuapp.com

* Use React, Redux, React Router
* Use Oxford Dictionaries API
* Use Material-UI library
* Follow Airbnb style guide

## TODO

* Add steps to pronounce a phonemic symbols
* Add meanings of word
* Add audio recorder for better practice
* Add a warning when adding a word does not contain the current phonemic symbols

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app) and [heroku-cra-node](https://github.com/mars/heroku-cra-node)

